package ru.t1.rleonov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.rleonov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.rleonov.tm.api.service.IDomainService;
import ru.t1.rleonov.tm.dto.request.*;
import ru.t1.rleonov.tm.dto.response.*;
import ru.t1.rleonov.tm.enumerated.Role;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.rleonov.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    @Autowired
    private IDomainService domainService;

    @Override
    @NotNull
    @WebMethod
    public ServerLoadDataBackupResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerLoadDataBackupRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.loadDataBackup();
        return new ServerLoadDataBackupResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerLoadDataBase64Response loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerLoadDataBase64Request request
    ) {
        check(request, Role.ADMIN);
        domainService.loadDataBase64();
        return new ServerLoadDataBase64Response();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerLoadDataBinaryResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerLoadDataBinaryRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.loadDataBinary();
        return new ServerLoadDataBinaryResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerLoadDataJsonFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerLoadDataJsonFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.loadDataJsonFasterXml();
        return new ServerLoadDataJsonFasterXmlResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerLoadDataJsonJaxBResponse loadDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerLoadDataJsonJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.loadDataJsonJaxB();
        return new ServerLoadDataJsonJaxBResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerLoadDataXmlFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerLoadDataXmlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.loadDataXmlFasterXml();
        return new ServerLoadDataXmlFasterXmlResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerLoadDataXmlJaxBResponse loadDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerLoadDataXmlJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.loadDataXmlJaxB();
        return new ServerLoadDataXmlJaxBResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerLoadDataYamlFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerLoadDataYamlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.loadDataYamlFasterXml();
        return new ServerLoadDataYamlFasterXmlResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerSaveDataBackupResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerSaveDataBackupRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.saveDataBackup();
        return new ServerSaveDataBackupResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerSaveDataBase64Response saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerSaveDataBase64Request request
    ) {
        check(request, Role.ADMIN);
        domainService.saveDataBase64();
        return new ServerSaveDataBase64Response();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerSaveDataBinaryResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerSaveDataBinaryRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.saveDataBinary();
        return new ServerSaveDataBinaryResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerSaveDataJsonFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerSaveDataJsonFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.saveDataJsonFasterXml();
        return new ServerSaveDataJsonFasterXmlResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerSaveDataJsonJaxBResponse saveDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerSaveDataJsonJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.saveDataJsonJaxB();
        return new ServerSaveDataJsonJaxBResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerSaveDataXmlFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerSaveDataXmlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.saveDataXmlFasterXml();
        return new ServerSaveDataXmlFasterXmlResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerSaveDataXmlJaxBResponse saveDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerSaveDataXmlJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.saveDataXmlJaxB();
        return new ServerSaveDataXmlJaxBResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public ServerSaveDataYamlFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerSaveDataYamlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        domainService.saveDataYamlFasterXml();
        return new ServerSaveDataYamlFasterXmlResponse();
    }

}
