package ru.t1.rleonov.tm.api.service.model;

import ru.t1.rleonov.tm.api.repository.model.IRepository;
import ru.t1.rleonov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
