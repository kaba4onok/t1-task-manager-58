package ru.t1.rleonov.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.api.repository.model.ISessionRepository;
import ru.t1.rleonov.tm.model.Session;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Nullable
    @Override
    public Session findOneById(@NotNull String id) {
        if (id.isEmpty()) return null;
        return entityManager.find(Session.class, id);
    }

}
