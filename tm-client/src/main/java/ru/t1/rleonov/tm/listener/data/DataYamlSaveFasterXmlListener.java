package ru.t1.rleonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.ServerSaveDataYamlFasterXmlRequest;
import ru.t1.rleonov.tm.event.ConsoleEvent;

@Component
public final class DataYamlSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-save-yaml-fasterxml";

    @NotNull
    private static final String DESCRIPTION = "Save data in yaml file using fasterxml.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlSaveFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ServerSaveDataYamlFasterXmlRequest request = new ServerSaveDataYamlFasterXmlRequest(getToken());
        getDomainEndpoint().saveDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
