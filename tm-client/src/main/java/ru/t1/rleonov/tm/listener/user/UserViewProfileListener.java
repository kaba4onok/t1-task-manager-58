package ru.t1.rleonov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.UserViewProfileRequest;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.dto.model.UserDTO;
import ru.t1.rleonov.tm.event.ConsoleEvent;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "user-view-profile";

    @NotNull
    private static final String DESCRIPTION = "View profile of current user.";

    @Override
    @EventListener(condition = "@userViewProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(getToken());
        @Nullable final UserDTO user = getUserEndpoint().viewUserProfile(request).getUser();
        if (user == null) return;
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
