package ru.t1.rleonov.tm.endpoint;

import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    /*@NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private static String user1Token;

    @Nullable
    private static String user2Token;

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUpClass() {
        user1Token = TestUtil.login(USER1);
        user2Token = TestUtil.login(USER2);
        adminToken = TestUtil.login(ADMIN);
    }

    @AfterClass
    public static void resetClass() {
        TestUtil.logout(user1Token);
        TestUtil.logout(user2Token);
        TestUtil.logout(adminToken);
    }

    @NotNull
    private static List<Project> getUserProjectList (@NotNull String token) {
        @NotNull final ProjectListRequest request = new ProjectListRequest(token);
        @Nullable final List<Project> projects = projectEndpoint.getProjectList(request).getProjects();
        if (projects == null) return Collections.emptyList();
        return projects;
    }

    @Before
    public void setUpMethod() {
        TestUtil.reloadData(adminToken);
    }

    @Test
    public void projectChangeStatusById () {
        @NotNull final String projectId = getUserProjectList(user1Token).get(0).getId();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(user1Token);
        request.setId(projectId);
        request.setStatus(Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.changeProjectStatusById(request)
                .getProject().getStatus());
    }

    @Test
    public void projectChangeStatusByIndex () {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(user1Token);
        request.setIndex(0);
        request.setStatus(Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectEndpoint.changeProjectStatusByIndex(request)
                .getProject().getStatus());
    }

    @Test
    public void projectClear() {
        Assert.assertNotNull(getUserProjectList(user1Token));
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(user1Token);
        projectEndpoint.clearProject(request);
        Assert.assertEquals(Collections.emptyList(), getUserProjectList(user1Token));
    }

    @Test
    public void projectCompleteById () {
        @NotNull final String projectId = getUserProjectList(user1Token).get(1).getId();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(user1Token);
        request.setId(projectId);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.completeProjectById(request)
                .getProject().getStatus());
    }

    @Test
    public void projectCompleteByIndex () {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(user1Token);
        request.setIndex(1);
        Assert.assertEquals(Status.COMPLETED, projectEndpoint.completeProjectByIndex(request)
                .getProject().getStatus());
    }

    @Test
    public void projectCreate() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(user1Token);
        request.setName(NEW_PROJECT.getName());
        request.setDescription(NEW_PROJECT.getDescription());
        projectEndpoint.createProject(request);
        Assert.assertEquals(NEW_PROJECT.getName(), projectEndpoint.createProject(request).getProject().getName());
    }

    @Test
    public void getProjectList() {
        @NotNull final List<Project> projects = getUserProjectList(user1Token);
        Assert.assertEquals(USER1_PROJECTS.size(), projects.size());
        Assert.assertEquals(USER1_PROJECTS.get(0).getName(), projects.get(0).getName());
        Assert.assertEquals(USER1_PROJECTS.get(1).getName(), projects.get(1).getName());
    }

    @Test
    public void projectRemoveById() {
        @NotNull final List<Project> projects = getUserProjectList(user1Token);
        final int size = USER1_PROJECTS.size();
        Assert.assertEquals(size, projects.size());
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(user1Token);
        request.setId(projects.get(0).getId());
        Assert.assertEquals(USER1_PROJECTS.get(0).getName(),
                projectEndpoint.removeProjectById(request).getProject().getName());
        Assert.assertEquals(size - 1, getUserProjectList(user1Token).size());
    }

    @Test
    public void projectRemoveByIndex() {
        @NotNull final List<Project> projects = getUserProjectList(user1Token);
        final int size = USER1_PROJECTS.size();
        Assert.assertEquals(size, projects.size());
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(user1Token);
        request.setIndex(0);
        Assert.assertEquals(USER1_PROJECTS.get(0).getName(),
                projectEndpoint.removeProjectByIndex(request).getProject().getName());
        Assert.assertEquals(size - 1, getUserProjectList(user1Token).size());
    }

    @Test
    public void projectShowById() {
        @NotNull final String projectId = getUserProjectList(user1Token).get(0).getId();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(user1Token);
        request.setId(projectId);
        Assert.assertEquals(PROJECT1.getName(), projectEndpoint.showProjectById(request).getProject().getName());
    }

    @Test
    public void projectShowByIndex() {
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(user1Token);
        request.setIndex(0);
        Assert.assertEquals(PROJECT1.getName(), projectEndpoint.showProjectByIndex(request).getProject().getName());
    }

    @Test
    public void projectStartById () {
        @NotNull final String projectId = getUserProjectList(user1Token).get(0).getId();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(user1Token);
        request.setId(projectId);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.startProjectById(request)
                .getProject().getStatus());
    }

    @Test
    public void projectStartByIndex () {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(user1Token);
        request.setIndex(0);
        Assert.assertEquals(Status.IN_PROGRESS, projectEndpoint.startProjectByIndex(request)
                .getProject().getStatus());
    }

    @Test
    public void projectUpdateById () {
        @NotNull final String projectId = getUserProjectList(user1Token).get(0).getId();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(user1Token);
        request.setId(projectId);
        request.setName(NEW_PROJECT.getName());
        request.setName(NEW_PROJECT.getDescription());
        Assert.assertEquals(NEW_PROJECT.getName(), projectEndpoint.updateProjectById(request)
                .getProject().getName());
    }

    @Test
    public void projectUpdateByIndex () {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(user1Token);
        request.setIndex(0);
        request.setName(NEW_PROJECT.getName());
        request.setName(NEW_PROJECT.getDescription());
        Assert.assertEquals(NEW_PROJECT.getName(), projectEndpoint.updateProjectByIndex(request)
                .getProject().getName());
    }*/

}
